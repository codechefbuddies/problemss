#include<stdio.h>
#include<iostream>
#include<string>
#include<string.h>
 
using namespace std;
 
int getNumberOfWord(char *a, int &str_len)
{
	int numOfWord = 1;
	for(int i =0;i<str_len;i++)
	{
		if(a[i]==' ')
			numOfWord++;
	}
	return numOfWord;
}
 
int main()
{
	
	int test;
	scanf("%d\n",&test);
	string inp_str;
	
	while(test--)
	{
		getline(cin,inp_str);
		int string_length = inp_str.length();
		char *char_str = new char [string_length+1];
		char *res_str = new char [string_length+1];
		strcpy(char_str,inp_str.c_str());
		int numOfWord = getNumberOfWord(char_str,string_length);
		if(numOfWord==1)
		{
			res_str[0]=(char_str[0]>='a')?char_str[0]-32:char_str[0];
			
			for(int i=1;i<string_length;i++)
			{
				res_str[i]=(char_str[i]<'a')?char_str[i]+32:char_str[i];
				
			}
			res_str[string_length]='\0';
			
		}
		else
		{
			int parsed_word =1;
			int j =0,i=0;
 
			for(i =0;i<string_length&& parsed_word<numOfWord;i++)
			{
			
				if(char_str[i]>='a')
				{
					res_str[j++]=char_str[i]-32;
				}
				else
				{
					res_str[j++]=char_str[i];
				}
				res_str[j++]='.';
				res_str[j++]=' ';
				
				parsed_word++;
			
				while(char_str[i]!=' ')
					i++;
				
			}
			res_str[j++]=(char_str[i]>='a')?char_str[i]-32:char_str[i];
			for(int k=i+1;k<string_length;k++)
				res_str[j++]=(char_str[k]<'a')?char_str[k]+32:char_str[k];
			res_str[j]='\0';
			
		}
		printf("%s\n",res_str);
		
	}
	return 0;
} 