#include<stdio.h>
#include<algorithm>
#include<vector>
#include<queue>
 
using namespace std;
struct data
{
	int d,t,s;
};
 
bool sortcmp(data data1, data data2)
{
	return(data1.d<data2.d);
}
 
bool heapcmp(data data1, data data2)
{
	return(data1.s<data2.s);
}
 
vector<data> teach_details;
 
int main()
{
	int test,N,D;
	scanf("%d",&test);
	
	while(test--)
	{
		scanf("%d%d",&N,&D);
		vector<data> teach_details;
		data temp;
		for(int i=0;i<N;i++)
		{
			scanf("%d%d%d",&temp.d,&temp.t,&temp.s);
			teach_details.push_back(temp);
		}
		sort(teach_details.begin(),teach_details.end(),sortcmp);
		
		vector<data> vec_heap;
		make_heap(vec_heap.begin(),vec_heap.end(),heapcmp);
		
		int num =0;
		for(int day=1;day<=D;day++)
		{
			while(teach_details[num].d==day && num<N)
			{
				vec_heap.push_back(teach_details[num]);
				push_heap(vec_heap.begin(),vec_heap.end(),heapcmp);
				num++;
			}
			if(vec_heap.size()>0)
			{
				data front_val = vec_heap.front();
				if((front_val.t)>0)
				{
					(front_val.t)--;
				}
				pop_heap(vec_heap.begin(),vec_heap.end(),heapcmp);
				vec_heap.pop_back();
				if((front_val.t)>0)
				{
					vec_heap.push_back(front_val);
					push_heap(vec_heap.begin(),vec_heap.end(),heapcmp);
				}
			}
		}
		long long int sum =0;
		for(auto itr=vec_heap.begin();itr!=vec_heap.end();itr++)
		{
			long long int temp1 = (long long int)(itr->t);
			long long int temp2 = (long long int)(itr->s);
			long long int temp3 = temp1*temp2;
			sum = sum + temp3;
		}
		printf("%lld\n",sum);
	}
	return 0;
} 