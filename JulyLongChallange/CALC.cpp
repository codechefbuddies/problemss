#include<stdio.h>
#include<cmath>
 
int main()
{
	long long int test;
	scanf("%lld",&test);
	long long int N,B;
	while(test--)
	{
		scanf("%lld%lld",&N,&B);
		long long int half = N/2;
		long long int mult = half/B;
		long long int max_val = 0;
		for(long long int i = mult-1;i<=mult+1;i++)
		{
			
			long long int ans= i*(N-(i*B));
			if(ans>max_val)
			{
				max_val = ans;
			}
		}
		printf("%lld\n",max_val);
		
		
	}
 
	return 0;
}  