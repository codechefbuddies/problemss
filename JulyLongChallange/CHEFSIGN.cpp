#include<stdio.h>
#include<string.h>
 
int main()
{
	int test;
	scanf("%d",&test);
	char inp_str[100005];
	while(test--)
	{
		int longest_common_sign=2;
		int current_longest_len = 2;
		scanf("%s",inp_str);
		int length = strlen(inp_str);
		char pre_char = inp_str[0];
		if(pre_char=='=')
		{
			longest_common_sign=1;
			current_longest_len=1;
		}
		for(int i=1;i<length;i++)
		{
			
				if(inp_str[i]==pre_char && inp_str[i]!='=')
				{
					current_longest_len++;
					longest_common_sign=(longest_common_sign>current_longest_len)?longest_common_sign:current_longest_len;
				}
				else if(inp_str[i]!='=')
				{
					current_longest_len=2;
					longest_common_sign=(longest_common_sign>current_longest_len)?longest_common_sign:current_longest_len;
					pre_char=inp_str[i];
				}
		}
		printf("%d\n",longest_common_sign);
	}
 
	return 0;
} 